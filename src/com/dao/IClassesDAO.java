package com.dao;

import com.vo.Classes;


import java.util.List;

public interface IClassesDAO {
    public List<Classes> findAllClasses() throws Exception;
}
