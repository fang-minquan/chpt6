package com.dao.impl;

import com.dao.IClassesDAO;
import com.db.ConnectionManager;
import com.vo.Classes;
import com.vo.Students;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class IClassesDAOImpl implements IClassesDAO {
    @Override
    public List<Classes> findAllClasses() throws Exception {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Classes> all = new ArrayList<>();
        try{
            con = ConnectionManager.getConnection();  //1.创建连接
            String strSql = "select * from classes";
            pstmt = con.prepareStatement(strSql);  //2.创建预处理语句总管
            rs = pstmt.executeQuery(); //3.语句总管执行SQL语句
            Classes cla=null;
            while(rs.next()){ //4.处理结果集
                cla=new Classes();
                cla.setClassId(rs.getInt("classId"));
                cla.setClassName(rs.getString("className"));
                all.add(cla);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            ConnectionManager.closeStatement(pstmt);
            ConnectionManager.closeConnection(con);
        }
        return all;
    }
}
