package com.dao;

import com.vo.Students;
import java.util.List;

public interface IStudentsDAO {
    public boolean insertStudents(Students students) throws Exception;
    public boolean deleteStudents(String studentId) throws Exception;
    public boolean updateStudents(Students students) throws Exception;
    public List<Students> findAllStudents() throws Exception;
    public Students findStudentsById(String studentId) throws Exception;
}
