package com.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginPart {

    public static boolean isExist(String sno)
    {
        String sql="Select * from stu where sno = ?";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement= ConnectionManager.getConnection().prepareStatement(sql);
            preparedStatement.setString(1,sno);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            ConnectionManager.closeStatement(preparedStatement);
        }
        return false;
    }
    public static String getPassword(String sno)
    {
        String sql="Select * from account where loginId = ?";
        if(!isExist(sno)) return null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement=ConnectionManager.getConnection().prepareStatement(sql);
            preparedStatement.setString(1,sno);
            String psd=null;
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next())
            {
                psd=resultSet.getString("loginPwd");
            }
            return psd;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            ConnectionManager.closeStatement(preparedStatement);
        }
        return null;
    }
    //密码加密
    public static String encryption(String password)
    {
        StringBuffer stringBuffer=new StringBuffer(password);
        for(int i=0;i<stringBuffer.length();i++)
        {
            stringBuffer.setCharAt(i, (char) (stringBuffer.codePointAt(i)+1820));
        }
        return stringBuffer.toString();
    }
    public static String decrypt(String encryptionPassword)
    {
        StringBuffer stringBuffer=new StringBuffer(encryptionPassword);
        for(int i=0;i<stringBuffer.length();i++)
        {
            stringBuffer.setCharAt(i, (char) (stringBuffer.codePointAt(i)-1820));
        }
        return stringBuffer.toString();
    }
}
