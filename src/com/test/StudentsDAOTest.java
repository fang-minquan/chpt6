package com.test;

import com.dao.impl.StudentsDAOImpl;
import com.vo.Students;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class StudentsDAOTest {
    @Test
    public void testInsertStudents() throws Exception {
        Students students = new Students();
        students.setStudentId("2018110106");
        students.setStudentName("乾隆");
        students.setStudentPwd("2018");
        students.setSex("男");
        students.setClassId(10201841);
        new StudentsDAOImpl().insertStudents(students);
    }

    @Test
    public void testDeleteStudents() throws Exception{
        String studentId = "2018110106";
        new StudentsDAOImpl().deleteStudents(studentId);
    }

    @Test
    public void testUpdateStudents() throws Exception{
        Students students = new Students();
        students.setStudentId("2018880123");
        students.setStudentName("玄烨");
        new StudentsDAOImpl().updateStudents(students);
    }

    @Test
    public void testFindStuById() throws Exception{
        String studentId = "2018110116";
        Students stu = new StudentsDAOImpl().findStudentsById(studentId);
        System.out.println(stu.toString());
    }

    @Test
    public void testFindAll() throws Exception{
        List<Students> all = new ArrayList<Students>();
        all = new StudentsDAOImpl().findAllStudents();
        for(Students stu:all){
            System.out.println(stu.toString());
        }
    }

    @Test
    public void testAmbiguous()throws Exception
    {
        List<Students>all=new StudentsDAOImpl().ambiguousSearch("110");
        for(Students stu:all){
            System.out.println(stu.toString());
        }
    }
}
