<%@ page import="com.vo.Students" %>
<%@ page import="com.factory.DAOFactory" %><%--
  Created by IntelliJ IDEA.
  User: 31002
  Date: 2022/4/16
  Time: 23:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>插入之后的验证界面</title>
</head>
<body>
    <%
        request.setCharacterEncoding("utf-8");
        Students students=new Students();
        students.setStudentId(request.getParameter("studentId"));
        students.setStudentName(request.getParameter("studentName"));
        students.setStudentPwd(request.getParameter("password"));
        students.setSex(request.getParameter("sex"));
        students.setClassId(Integer.parseInt(request.getParameter("classId")));
    %>
    <% boolean flag=DAOFactory.getStudentsDAOInstance().insertStudents(students); if(flag){%>
    <strong style="font-size: 30px"><%="成功插入姓名为:"+students.getStudentName()+"的学生记录"%></strong>
    <%}else{%>
    <strong style="font-size: 30px"><%="插入姓名为:"+students.getStudentName()+"的学生记录失败了"%></strong>
    <%}%>
    <br>
    <br>
    <strong style="font-size: 30px">返回<a href="showStudents.jsp">学生信息</a>页面！</strong>
</body>
</html>
