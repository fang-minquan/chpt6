<%@ page import="com.vo.Students" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="com.factory.DAOFactory" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>显示所有学生信息</title>
</head>
<body>
<form action="ambiguousSearch.jsp" method="get" >
    <input type="text" align="center" width="200px" name="ambiguous_search">
    <input type="submit" value="模糊查询">
</form>

<table border="2" align="center" align="70">


    <caption>显示所有学生信息</caption>
    <tr><th>学号</th><th>姓名</th><th>密码</th><th>性别</th><th>班级号</th><th>修改</th><th>删除</th></tr>
    <%
        List<Students> all=new ArrayList<Students>();
        all= DAOFactory.getStudentsDAOInstance().findAllStudents();
        if(all.size()!=0){
            for(int i = 0;i < all.size();i++){
                Students stu = (Students)all.get(i);
    %>
    <tr>
        <td><%=stu.getStudentId() %></td>
        <td><%=stu.getStudentName() %></td>
        <td><%=stu.getStudentPwd() %></td>
        <td><%=stu.getSex() %></td>
        <td><%=stu.getClassId() %></td>
        <td><a href="updateStudents.jsp?studentId=<%=stu.getStudentId() %>">修改</a></td>
        <td><a href="deleteStudents.jsp?studentId=<%=stu.getStudentId() %>">删除</a></td>
    </tr>
    <%
            } //end for
        }//end if
    %>
</table>
<a href="insertStudents.jsp">插入一条学生的信息</a>
</body>
</html>
