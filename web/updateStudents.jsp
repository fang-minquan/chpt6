<%@ page import="java.sql.Connection" %>
<%@ page import="com.db.ConnectionManager" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSetMetaData" %>
<%@ page import="com.vo.Classes" %>
<%@ page import="com.factory.DAOFactory" %>
<%@ page import="java.util.List" %>
<%@ page import="com.vo.Students" %><%--
  Created by IntelliJ IDEA.
  User: 31002
  Date: 2022/4/16
  Time: 19:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改学生的界面</title>
</head>
<body>
<%request.setCharacterEncoding("utf-8");%>

<p align="center"><strong>修改一条学生的信息</strong></p>
    <%
        String studentId=request.getParameter("studentId");
        Students students=DAOFactory.getStudentsDAOInstance().findStudentsById(studentId);
    %>
<form action="doUpdateStu.jsp" method="get">
    <table border="1px" align="center" style="border-collapse: collapse">
        <tr>
            <td width="200px" height="30px" style="text-align: center;"><strong>学号：</strong></td>
            <td width="300px">
                <input  type="text"  value="<%=students.getStudentId()%>" readonly="readonly" name="studentId"  maxlength="10" style="border: 0;width: 300px;height: 30px;font-size: 30px">
            </td>
        </tr>
        <tr>
            <td width="200px" style="text-align: center;"><strong>姓名：</strong></td>
            <td width="300px"><input  type="text" value="<%=students.getStudentName()%>" name="studentName" maxlength="8" style="border: 0;width: 300px;height: 30px;font-size: 30px"></td>
        </tr>
        <tr>
            <td width="200px" style="text-align: center;"><strong>密码：</strong></td>
            <td width="300px"><input  type="password" name="password" value="<%=students.getStudentPwd()%>" maxlength="18" style="border: 0;width: 300px;height: 30px;font-size: 30px"></td>
        </tr>
        <tr>
            <td width="200px" style="text-align: center;"><strong>性别：</strong></td>
            <td>
                <label><input type="radio" name="sex" value="男" id="radio_man"  >男</label>
                <label><input type="radio" name="sex" value="女" id="radio_woman" >女</label>
            </td>
        </tr>
        <tr>
            <td width="200px" style="text-align: center;"><strong>班级：</strong></td>
            <td>
                <select name="classId">
                    <%List<Classes> all= DAOFactory.getClassesDAOInstance().findAllClasses();%>
                    <%for(int i=0;i< all.size();i++){%>
                    <option value="<%=all.get(i).getClassId()%>" id="<%=all.get(i).getClassId()%>"><%=all.get(i).getClassName()%></option>
                    <%}%>
                </select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit"></td>
        </tr>
    </table>
</form>
<script>
    function onHim() {
        alert("你好啊 man");
    }
    function onHer() {
        alert("你好啊 woman");
    }

    let one="<%=students.getSex()%>";
    //alert(one);
    if(one=="男")
    {
        document.getElementById("radio_man").checked=true;
    }else
    {
        document.getElementById("radio_woman").checked=true;
    }
    let two="<%=students.getClassId()%>"
    document.getElementById(two).selected=true;
</script>
</body>
</html>
