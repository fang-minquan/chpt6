<%@ page import="java.util.List" %>
<%@ page import="com.vo.Students" %>
<%@ page import="com.factory.DAOFactory" %>
<%@ page import="com.dao.impl.StudentsDAOImpl" %><%--
  Created by IntelliJ IDEA.
  User: 31002
  Date: 2022/4/17
  Time: 18:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>模糊查询界面</title>
</head>
<body>
    <%
        String information=request.getParameter("ambiguous_search");
        List<Students>all= StudentsDAOImpl.ambiguousSearch(information);
        if(all==null||all.size()==0){
    %>
    <strong style="font-size: 30px;"><%="没找到相关的信息"%></strong>
    <%}else{%>
    <table border="1px" align="center">
        <caption ><strong style="font-size: 30px">显示所有学生信息</strong></caption>
        <tr><th>学号</th><th>姓名</th><th>密码</th><th>性别</th><th>班级号</th>
        <%for(int i = 0;i < all.size();i++){
            Students stu = all.get(i);%>
        <tr>
        <td><%=stu.getStudentId() %></td>
        <td><%=stu.getStudentName() %></td>
        <td><%=stu.getStudentPwd() %></td>
        <td><%=stu.getSex() %></td>
        <td><%=stu.getClassId() %></td>
        </tr>
        <%}%>
    </table>
    <%}%>
    <br>
    <br>
    <strong style="font-size: 30px">返回<a href="showStudents.jsp">学生信息</a>页面！</strong>
</body>
</html>
